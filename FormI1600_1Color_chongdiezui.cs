﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintRuler
{
    public partial class FormI1600_1Color_chongdiezui : Form
    {
        private const int width = 400;
        private const int height = 3400;
        private const int count = 30;
        private const int mark_line_length = 100;
        private const int Offset = 200;// 水平线的位置，基本图偏移量
        private const int base_interval = 20;//校准长线的间隔
        private int startPosY = 0;
        private int Pos = 0;// 第几列喷头的数据
        private string str_Pos = "pos1";
        private string str_CMYK = "ABCD";

        public FormI1600_1Color_chongdiezui()
        {
            InitializeComponent();
        }
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            PictureBox picbox = (PictureBox)sender;
            Size size = new Size(width, height);
            picbox.Size = size;

            Graphics g = e.Graphics;
            g.Clear(Color.White);
            Pen pen = new Pen(Color.Black);
            Font font = new Font("Microsoft YaHei", 48, FontStyle.Regular);
            Font font_base_line = new Font("Microsoft YaHei", 10, FontStyle.Bold);
            Matrix matrix = new Matrix();

            // 开始水平线
            for (int i = 0; i < 100; i++)
            {
                g.DrawLine(pen, 0, startPosY + Offset - i, width - 1, startPosY + Offset - i);
            }
            g.DrawString(str_CMYK, new Font("Arial", 96, FontStyle.Bold), Brushes.White, -10, startPosY + Offset - 119);

            // 基本长线
            if(str_Pos == "pos4")
            {
                for (int i = 0; i < 7; i++)
                {
                    if (i != 0)
                    {
                        matrix = new Matrix();
                        matrix.RotateAt(90, new PointF(width - 1 - i * base_interval, startPosY + Offset + i * Offset));

                        // 应用旋转变换
                        g.Transform = matrix;

                        g.DrawString("+" + (base_interval * i).ToString(), font_base_line, Brushes.Black, width - 1 - i * base_interval/2, startPosY + Offset + i * (Offset - 10)-10);
                        // 恢复默认的变换
                        g.ResetTransform();
                    }

                    g.DrawLine(pen, width - 1 - i * base_interval/2, startPosY + Offset, width - 1 - i * base_interval/2, startPosY + height - 1 - Offset);
                }
            }

            // 结束水平线
            for (int i = 0; i < 100; i++)
            {
                g.DrawLine(pen, 0, startPosY + height - 1 - Offset + i, width - 1, startPosY + height - 1 - Offset + i);
            }
            g.DrawString(str_CMYK, new Font("Arial", 96, FontStyle.Bold), Brushes.White, -10, startPosY + height - Offset - 22);

            // 校准短线
            for (int i = 0; i < count; i++)
            {
                if(i%2==Pos)
                {
                    matrix = new Matrix();
                    matrix.RotateAt(90, new PointF(15 + i, startPosY + Offset + i * mark_line_length));

                    // 应用旋转变换
                    g.Transform = matrix;
                    g.DrawString((i + 1).ToString(), font, Brushes.Black, 7 + i, startPosY + Offset + i * mark_line_length - 75);
                    // 恢复默认的变换
                    g.ResetTransform();

                    g.DrawLine(pen, i/2, startPosY + Offset + i * mark_line_length, i/2, startPosY + Offset + (i + 1) * mark_line_length);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            pictureBox1.DrawToBitmap(bitmap, pictureBox1.ClientRectangle);
            // 32转1bit
            Bitmap newbitmap = bitmap.Clone(new Rectangle(0, 0, bitmap.Width, bitmap.Height), PixelFormat.Format1bppIndexed);

            //bitmap.Save("output\\scale_ruler_32bit.bmp", ImageFormat.Bmp);
            newbitmap.Save($"output\\I1600-1COLOR\\chongdiezui_{width}X{height}_{str_Pos}_1bit.bmp", ImageFormat.Bmp);

            bitmap.Dispose();
            newbitmap.Dispose();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    Pos = 0;
                    str_Pos = "pos1";
                    break;
                case 1:
                    Pos = 1;
                    str_Pos = "pos2";
                    break;
                case 2:
                    Pos= 0;
                    str_Pos = "pos3";
                    break;
                case 3:
                    Pos = 1;
                    str_Pos = "pos4";
                    break;
                default:
                    startPosY = 0;
                    str_Pos = "pos1";
                    break;
            }
            pictureBox1.Invalidate();
        }
        

    }
}
