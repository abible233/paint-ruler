﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintRuler
{
    public partial class FormI1600_1color_zhuangtaitu : Form
    {
        [DllImport("bmpModifyBitDepth.dll", EntryPoint = "ModifyBMPBitsPerPixel", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        public static extern int ModifyBMPBitsPerPixel(string BMPFilePath, string NewBMPFilePath_1bit);

        //[DllImport("bmpModifyBitDepth.dll", EntryPoint = "square2", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        //public static extern int square1(int x);

        private const int width = 400;
        private const int height = 12600;// 3000pixels x 4

        private const int deviation_angle_area_height = 1000;// 角度区域总高度
        private const int angle_area_interval = 46;// 角度两块之间的间隔 1mm
        private const int angle_base_length = 151;// 角度基准线
        private const int angle_check_length = 250;// 角度校准线
        private bool m_bIsAngleBase = true;// 当前是否是基准线
        private bool m_bShort;

        private const int interval = 64;// 一组小线段所占的高度，用于和nCount相乘算Y位置
        private const int short_line_length = 80;// 长短线看夹的情况，短线长度
        private const int long_line_length = 150;// 长短线看夹的情况，长线长度
        private const int tixing_space = 4;// 梯形线的间隔


        private int startPosY = 0 + deviation_angle_area_height;
        private int startPosY_Angle = 0 + angle_area_interval / 2;
        private int nOddOrEven = 0;

        private string str_CMYK = "Head";
        private string str_checkPos = "Base";

        public FormI1600_1color_zhuangtaitu()
        {
            InitializeComponent();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            int nPHCnt = 1;
            Size picture_size = new Size(width* nPHCnt, height + deviation_angle_area_height);
            pictureBox1.Size = picture_size;
            int nCount = 0;

            int angle_area_one_height = (deviation_angle_area_height - angle_area_interval * 2) / 2;// 单个角度块的高度(不含间隔)

            Graphics g = e.Graphics;
            g.Clear(Color.White);
            Pen pen = new Pen(Color.Black);
            Font font = new Font("Arial", 90, FontStyle.Bold);
            Pen pen_dot = new Pen(Color.Black);
            //pen_dot.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            SolidBrush brush = new SolidBrush(Color.Black);

            // 角度线区域作图
            if (m_bIsAngleBase)
            {
                // 基础线
                // 横线
                for (int j = 0; j < 2; j++)
                {
                    g.DrawLine(pen, 0, startPosY_Angle, width - 1, startPosY_Angle);
                    g.DrawLine(pen, 0, startPosY_Angle + angle_area_one_height, width - 1, startPosY_Angle + angle_area_one_height);
                }
                // 刻度线
                for (int j = 0; j < width; j += 50)
                {
                    g.DrawLine(pen, j, startPosY_Angle, j, startPosY_Angle + angle_base_length);
                    g.DrawLine(pen, j, startPosY_Angle + angle_area_one_height, j, startPosY_Angle - angle_base_length + angle_area_one_height);
                }
            }
            else
            {
                // 校准线
                for (int j = 0; j < width; j += 50)
                {
                    g.DrawLine(pen, j, startPosY_Angle + (angle_area_one_height - angle_check_length) / 2, j, startPosY_Angle + (angle_area_one_height + angle_check_length) / 2);
                }
            }

            g.DrawLine(pen, 0, startPosY, width*nPHCnt - 1, startPosY);
            if(m_bShort)
            {
                for (int nodd = 0; nodd < 4; nodd++)
                {
                    int startPosY_temp = height / 4 * nodd + deviation_angle_area_height;
                    if (nodd % 2 != nOddOrEven)
                        continue;
                    for (int x = 0; x < width * nPHCnt; x += width)
                    {
                        nCount = 0;
                        // 短线夹长线
                        for (int i = 0; i < width - 72; i += 72)
                        {
                            g.DrawLine(pen_dot, x + i, startPosY_temp + nCount * interval + 3, x + i, startPosY_temp + nCount * interval + 3 + short_line_length);
                            g.DrawLine(pen_dot, x + i + 36, startPosY_temp + nCount * interval + 3, x + i + 36, startPosY_temp + nCount * interval + 3 + short_line_length);
                        }

                        nCount = 3;
                        for (int i = 0; i < width; i += 40)
                        {
                            g.DrawLine(pen_dot, x + i, startPosY_temp + nCount * interval + 3, x + i, startPosY_temp + nCount * interval + 3 + short_line_length);
                            g.DrawLine(pen_dot, x + i + 14, startPosY_temp + nCount * interval + 3, x + i + 14, startPosY_temp + nCount * interval + 3 + short_line_length);
                        }

                        nCount = 6;
                        for (int i = 0; i < width - 24; i += 24)
                        {
                            g.DrawLine(pen_dot, x + i, startPosY_temp + nCount * interval + 3, x + i, startPosY_temp + nCount * interval + 3 + short_line_length);
                            g.DrawLine(pen_dot, x + i + 6, startPosY_temp + nCount * interval + 3, x + i + 6, startPosY_temp + nCount * interval + 3 + short_line_length);
                        }
                    }
                }
            }
            else
            {
                for (int nodd = 0; nodd < 4; nodd++)
                {
                    int startPosY_temp = height / 4 * nodd + deviation_angle_area_height;
                    if (nodd % 2 == nOddOrEven)
                        continue;
                    for (int x = 0; x < width * nPHCnt; x += width)
                    {
                        nCount = 0;
                        // 短线夹长线
                        for (int i = 0; i < width - 72; i += 72)
                        {
                            g.DrawLine(pen_dot, x + i + 18, startPosY_temp + nCount * interval + 3, x + i + 18, startPosY_temp + nCount * interval + 3 + long_line_length);
                        }

                        nCount = 3;
                        for (int i = 0; i < width; i += 40)
                        {
                            g.DrawLine(pen_dot, x + i + 7, startPosY_temp + nCount * interval + 3, x + i + 7, startPosY_temp + nCount * interval + 3 + long_line_length);
                        }

                        nCount = 6;
                        for (int i = 0; i < width - 24; i += 24)
                        {
                            g.DrawLine(pen_dot, x + i + 3, startPosY_temp + nCount * interval + 3, x + i + 3, startPosY_temp + nCount * interval + 3 + long_line_length);
                        }
                    }
                }
            }


            // 梯形线
            nCount = 10;
            g.DrawLine(pen, 0, startPosY + nCount * interval - 50, width*nPHCnt - 1, startPosY + nCount * interval - 50);
            int tixingxian_height = interval * 12 / tixing_space;// 梯形块占用多少个小宽度，再除以8得高度
            for (int i = 0; i < width * nPHCnt; i += tixing_space)
            {
                for (int j = 0; j < tixing_space; j++)
                {
                    g.DrawLine(pen_dot, i + j, startPosY + nCount * interval + tixingxian_height * j + 1, i + j, startPosY + nCount * interval + tixingxian_height * (j + 1));
                }
            }

            // 横直线 宽分别为1 2 3 4像素
            nCount = 23;
            int space = 15;
            // 宽度为1
            int line_offset = 10;// 直线相对于一整个interval的偏移
            int line_width = 1;
            for (int i = 0; i < line_width; i++)
            {
                g.DrawLine(pen, 0, startPosY + nCount * interval + line_offset + i, width * nPHCnt - 1, startPosY + nCount * interval + line_offset + i);
            }
            //宽度为2 
            line_offset += space + line_width - 1;
            line_width = 2;
            for (int i = 0; i < line_width; i++)
            {
                g.DrawLine(pen, 0, startPosY + nCount * interval + line_offset + i, width * nPHCnt - 1, startPosY + nCount * interval + line_offset + i);
            }
            //宽度为3
            line_offset += space + line_width - 1;
            line_width = 3;
            for (int i = 0; i < line_width; i++)
            {
                g.DrawLine(pen, 0, startPosY + nCount * interval + line_offset + i, width * nPHCnt - 1, startPosY + nCount * interval + line_offset + i);
            }
            //宽度为4

            line_offset += space + line_width - 1;
            line_width = 4;
            for (int i = 0; i < line_width; i++)
            {
                g.DrawLine(pen, 0, startPosY + nCount * interval + line_offset + i, width * nPHCnt - 1, startPosY + nCount * interval + line_offset + i);
            }

            // 横虚线
            int dashlength = 4;// 虚线白加黑的总长度
            int dashlength_black = 2;// 虚线黑线长度
            //宽度为1
            line_offset += space + line_width - 1;
            line_width = 1;
            for (int j = 0; j < line_width; j++)
            {
                for (int i = 0; i < width * nPHCnt; i += dashlength)
                {
                    g.DrawLine(pen, i, startPosY + nCount * interval + line_offset + j, i + dashlength_black, startPosY + nCount * interval + line_offset + j);
                }
            }
            // 宽度为2
            line_offset += space + line_width - 1;
            line_width = 2;
            for (int j = 0; j < line_width; j++)
            {
                for (int i = 0; i < width * nPHCnt; i += dashlength)
                {
                    g.DrawLine(pen, i, startPosY + nCount * interval + line_offset + j, i + dashlength_black, startPosY + nCount * interval + line_offset + j);
                }
            }
            // 宽度为1  增加需线条长度
            line_offset += space + line_width - 1;
            line_width = 1;
            dashlength = 12;
            dashlength_black = 5;
            for (int j = 0; j < line_width; j++)
            {
                for (int i = 0; i < width * nPHCnt; i += dashlength)
                {
                    g.DrawLine(pen, i, startPosY + nCount * interval + line_offset + j, i + dashlength_black, startPosY + nCount * interval + line_offset + j);
                }
            }

            // 虚线宽度2
            line_offset += space + line_width - 1;
            line_width = 2;
            dashlength = 12;
            dashlength_black = 5;
            for (int j = 0; j < line_width; j++)
            {
                for (int i = 0; i < width * nPHCnt; i += dashlength)
                {
                    g.DrawLine(pen, i, startPosY + nCount * interval + line_offset + j, i + dashlength_black, startPosY + nCount * interval + line_offset + j);
                }
            }


            nCount = 25;
            int color_block_interval = 4;// 色块线宽加间隔

            nCount = 27;
            color_block_interval = 6;
            for (int i = 0; i < width * nPHCnt; i += color_block_interval)
            {
                for (int j = startPosY + nCount * interval; j <= startPosY + nCount * interval + interval * 2; j += 6)
                {
                    g.FillRectangle(brush, i, j, 1, 1);
                }

                //g.DrawLine(pen_dot, i, startPosY + nCount * interval, i, startPosY + nCount * interval + interval * 2);
            }
            nCount = 29;
            color_block_interval = 4;
            for (int i = 0; i < width * nPHCnt; i += color_block_interval)
            {
                g.DrawLine(pen_dot, i, startPosY + nCount * interval, i, startPosY + nCount * interval + interval * 2);
            }
            nCount = 31;
            color_block_interval = 1;
            for (int i = 0; i < width * nPHCnt; i += color_block_interval)
            {
                g.DrawLine(pen, i, startPosY + nCount * interval, i, startPosY + nCount * interval + interval * 2);
            }

            for (int x =0;x<nPHCnt;x++)
            {
                g.DrawString(str_CMYK , font, Brushes.White, x*width +width* 0.25f, startPosY + nCount * interval + 5);
            }


            // 末尾分隔线
            for (int i = 1; i < 3; i++)
            {
                g.DrawLine(pen, 0, startPosY + height / 4 - i, width * nPHCnt - 1, startPosY + height / 4 - i);
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            pictureBox1.DrawToBitmap(bitmap, pictureBox1.ClientRectangle);
            // 32转1bit
            Bitmap newbitmap = bitmap.Clone(new Rectangle(0, 0, bitmap.Width, bitmap.Height), PixelFormat.Format1bppIndexed);
            bitmap.Dispose();
            newbitmap.Save($"output\\I1600-1COLOR\\penkong_{width}x{height}_{str_CMYK}_{str_checkPos}_1bit.bmp", ImageFormat.Bmp);


            newbitmap.Dispose();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    startPosY = 0 + deviation_angle_area_height;
                    str_CMYK = "CH1";
                    break;
                case 1:
                    startPosY = height / 4 * 1 + deviation_angle_area_height;
                    str_CMYK = "CH2";
                    break;
                case 2:
                    startPosY = height / 4 * 2 + deviation_angle_area_height;
                    str_CMYK = "CH3";
                    nOddOrEven = 0;
                    break;
                case 3:
                    startPosY = height / 4 * 3 + deviation_angle_area_height;
                    str_CMYK = "CH4";
                    break;
                default:
                    startPosY = 0 + deviation_angle_area_height;
                    str_CMYK = "CH1";
                    break;
            }
            pictureBox1.Invalidate();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox2.SelectedIndex)
            {
                case 0:
                    startPosY_Angle = 0 + angle_area_interval / 2;
                    str_checkPos = "Base1";
                    m_bIsAngleBase = true;
                    break;
                case 1:
                    startPosY_Angle = deviation_angle_area_height / 2 * 1 + angle_area_interval / 2;
                    str_checkPos = "Base2";
                    m_bIsAngleBase = true;
                    break;
                case 2:
                    startPosY_Angle = 0 + angle_area_interval / 2;
                    str_checkPos = "Check1";
                    m_bIsAngleBase = false;
                    break;
                case 3:
                    startPosY_Angle = deviation_angle_area_height / 2 * 1 + angle_area_interval / 2;
                    str_checkPos = "Check2";
                    m_bIsAngleBase = false;
                    break;
                default:
                    startPosY_Angle = 0 + angle_area_interval / 2;
                    m_bIsAngleBase = true;
                    break;
            }
            pictureBox1.Invalidate();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            m_bShort = checkBox1.Checked;
            pictureBox1.Invalidate();
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                nOddOrEven = 1;
            }
            else
            {
                nOddOrEven = 0;
            }
            pictureBox1.Invalidate();
        }
    }
}
