﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintRuler
{
    public partial class FormKJ4B1200_2H_status : Form
    {
        [DllImport("bmpModifyBitDepth.dll", EntryPoint = "ModifyBMPBitsPerPixel", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Cdecl)]
        public static extern int ModifyBMPBitsPerPixel(string BMPFilePath, string NewBMPFilePath_1bit);

        private const int width = 5116;// 1200:5116  600:2656 -49-63   后续是减49*2  2558
        private bool b_600 = false;
        private const int height = 3000;// 3000pixels x 4  12600

        private const int deviation_angle_area_height = 1000;// 角度区域总高度
        private const int angle_area_interval = 46;// 角度两块之间的间隔 1mm
        private const int angle_base_length = 151;// 角度基准线
        private const int angle_check_length = 250;// 角度校准线
        private bool m_bIsAngleBase = true;// 当前是否是基准线

        private const int interval = 64;// 一组小线段所占的高度，用于和nCount相乘算Y位置
        private const int short_line_length = 80;// 长短线看夹的情况，短线长度
        private const int long_line_length = 150;// 长短线看夹的情况，长线长度
        private const int tixing_space = 16;// 梯形线的间隔


        private int startPosY = 0;
        private string str_CMYK = "ABCDEFGH1234567890";
        //private int startPosY = 0 + deviation_angle_area_height;
        private int startPosY_Angle = 0 + angle_area_interval / 2;
        //private string str_CMYK = "M";
        private string str_checkPos = "Base";
        public FormKJ4B1200_2H_status()
        {
            InitializeComponent();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Size picture_size = new Size(width, height /*+ deviation_angle_area_height*/);
            pictureBox1.Size = picture_size;
            int nCount = 0;
            int angle_area_one_height = (deviation_angle_area_height - angle_area_interval * 2) / 2;// 单个角度块的高度(不含间隔)

            Graphics g = e.Graphics;
            g.Clear(Color.White);
            Pen pen = new Pen(Color.Black);
            Font font = new Font("Arial", 90, FontStyle.Bold);
            Pen pen_dot = new Pen(Color.Black);
            pen_dot.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            SolidBrush brush = new SolidBrush(Color.Black);

            // 角度线区域作图
            //if (m_bIsAngleBase)
            //{
            //    // 基础线
            //    // 横线
            //    for (int j = 0; j < 2; j++)
            //    {
            //        g.DrawLine(pen, 0, startPosY_Angle, width - 1, startPosY_Angle);
            //        g.DrawLine(pen, 0, startPosY_Angle + angle_area_one_height, width - 1, startPosY_Angle + angle_area_one_height);
            //    }
            //    // 刻度线
            //    for (int j = 0; j < width; j += 50)
            //    {
            //        g.DrawLine(pen, j, startPosY_Angle, j, startPosY_Angle + angle_base_length);
            //        g.DrawLine(pen, j, startPosY_Angle + angle_area_one_height, j, startPosY_Angle - angle_base_length + angle_area_one_height);
            //    }
            //}
            //else
            //{
            //    // 校准线
            //    for (int j = 0; j < width; j += 50)
            //    {
            //        g.DrawLine(pen, j, startPosY_Angle + (angle_area_one_height - angle_check_length) / 2, j, startPosY_Angle + (angle_area_one_height + angle_check_length) / 2);
            //    }
            //}


            g.DrawLine(pen, 0, startPosY, width - 1, startPosY);
            // 小三角
            int nTri_short = 2;
            int nTri_long = 3;
            // 短线夹长线
            int n_X1_interval = 1;
            if (b_600)
            {
                List<int> startX = new List<int>() { 24, 696, 1352, 2024 };
                for (int v = 0; v < 4; v++)
                {
                    int Start_x = startX[v];
                    for (int i = Start_x; i < Start_x + 520; i += 32)
                    {
                        g.DrawLine(pen, i, startPosY + nCount * interval, i, startPosY + nCount * interval + nTri_short);
                        g.DrawLine(pen, i + 1, startPosY + nCount * interval, i + 1, startPosY + nCount * interval + nTri_long);
                        g.DrawLine(pen, i + 2, startPosY + nCount * interval, i + 2, startPosY + nCount * interval + nTri_short);
                    }

                }
            }
            else
            {
                nTri_short = 26;
                nTri_long = 56;
                for (int i = 36; i < width; i += 64)
                {
                    g.DrawLine(pen_dot, i, startPosY + nCount * interval+3 , i, startPosY + nCount * interval  + nTri_short + 3);
                    g.DrawLine(pen_dot, i + n_X1_interval * 1, startPosY + nCount * interval +3, i + n_X1_interval * 1, startPosY + nCount * interval + nTri_long + 3);
                    g.DrawLine(pen_dot, i + n_X1_interval * 2, startPosY + nCount * interval + 3, i + n_X1_interval * 2, startPosY + nCount * interval + nTri_short + 3);
                }
               
                n_X1_interval = 2;
                for (int i = 41; i < width; i += 64)
                {
                    g.DrawLine(pen_dot, i, startPosY + nCount * interval + 3 + 100, i, startPosY + nCount * interval + nTri_short + 3 + 100);
                    g.DrawLine(pen_dot, i + n_X1_interval * 1, startPosY + nCount * interval + 3 + 100, i + n_X1_interval * 1, startPosY + nCount * interval + nTri_long + 3 + 100);
                    g.DrawLine(pen_dot, i + n_X1_interval * 2, startPosY + nCount * interval + 3 + 100, i + n_X1_interval * 2, startPosY + nCount * interval + nTri_short + 3 + 100);
                }
                
                n_X1_interval = 3;
                for (int i = 45; i < width; i += 64)
                {
                    g.DrawLine(pen_dot, i, startPosY + nCount * interval + 3 + 200, i, startPosY + nCount * interval + nTri_short + 3 + 200);
                    g.DrawLine(pen_dot, i + n_X1_interval * 1, startPosY + nCount * interval + 3 + 200, i + n_X1_interval * 1, startPosY + nCount * interval + nTri_long + 3 + 200);
                    g.DrawLine(pen_dot, i + n_X1_interval * 2, startPosY + nCount * interval + 3 + 200, i + n_X1_interval * 2, startPosY + nCount * interval + nTri_short + 3 + 200);
                }
            }

            n_X1_interval = 4;
            if (b_600)
            {
                n_X1_interval = 5;
                List<int> startX = new List<int>() { 2, 674, 1330, 2002 };
                for(int v = 0; v < 4; v++)
                {
                    int Start_x = startX[v];
                    for (int i =  Start_x; i < Start_x+520 ; i += 32)
                    {
                        g.DrawLine(pen_dot, i, startPosY + nCount * interval + 3, i, startPosY + nCount * interval + 3 + short_line_length);
                        g.DrawLine(pen_dot, i + n_X1_interval, startPosY + nCount * interval + 3, i + n_X1_interval, startPosY + nCount * interval + 3 + long_line_length);
                        g.DrawLine(pen_dot, i + n_X1_interval*2, startPosY + nCount * interval + 3, i + n_X1_interval*2, startPosY + nCount * interval + 3 + short_line_length);
                    }

                }
            }
            else
            {
                for (int i = 10; i < width; i += 64)
                {
                    g.DrawLine(pen_dot, i, startPosY + nCount * interval + 3, i, startPosY + nCount * interval + 3 + short_line_length);
                    g.DrawLine(pen_dot, i + n_X1_interval * 1, startPosY + nCount * interval + 3, i + n_X1_interval * 1, startPosY + nCount * interval + 3 + long_line_length);
                    g.DrawLine(pen_dot, i + n_X1_interval * 2, startPosY + nCount * interval + 3, i + n_X1_interval * 2, startPosY + nCount * interval + 3 + short_line_length);
                }
            }

            nCount = 3;
            n_X1_interval = 8;
            if (b_600)
            {
                n_X1_interval = 11;
                List<int> startX = new List<int>() {6, 678, 1334, 2006 };
                for (int v = 0; v < 4; v++)
                {
                    int Start_x = startX[v];
                    for (int i =  Start_x; i < Start_x + 490 ; i += 48)
                    {
                        g.DrawLine(pen_dot, i, startPosY + nCount * interval + 3, i, startPosY + nCount * interval + 3 + short_line_length);
                        g.DrawLine(pen_dot, i + n_X1_interval, startPosY + nCount * interval + 3, i + n_X1_interval, startPosY + nCount * interval + 3 + long_line_length);
                        g.DrawLine(pen_dot, i + n_X1_interval*2, startPosY + nCount * interval + 3, i + n_X1_interval*2, startPosY + nCount * interval + 3 + short_line_length);
                    }

                }
            }
            else
            {
                for (int i = 13; i < width; i += 64)
                {
                    if (width - i < 80)
                        break;
                    g.DrawLine(pen_dot, i, startPosY + nCount * interval + 3, i, startPosY + nCount * interval + 3 + short_line_length);
                    g.DrawLine(pen_dot, i + n_X1_interval, startPosY + nCount * interval + 3, i + n_X1_interval, startPosY + nCount * interval + 3 + long_line_length);
                    g.DrawLine(pen_dot, i + n_X1_interval * 2, startPosY + nCount * interval + 3, i + n_X1_interval * 2, startPosY + nCount * interval + 3 + short_line_length);
                }
            }

            nCount = 6;
            n_X1_interval = 24;
            if (b_600)
            {
                n_X1_interval = 21;
                List<int> startX = new List<int>() { 10, 682, 1338, 2010 };
                for (int v = 0; v < 4; v++)
                {
                    int Start_x = startX[v];
                    for (int i =  Start_x; i < Start_x + 490 ; i += 96)
                    {
                        g.DrawLine(pen_dot, i, startPosY + nCount * interval + 3, i, startPosY + nCount * interval + 3 + short_line_length);
                        g.DrawLine(pen_dot, i + n_X1_interval, startPosY + nCount * interval + 3, i + n_X1_interval, startPosY + nCount * interval + 3 + long_line_length);
                        g.DrawLine(pen_dot, i + n_X1_interval*2, startPosY + nCount * interval + 3, i + n_X1_interval*2, startPosY + nCount * interval + 3 + short_line_length);
                    }

                }
            }
            else
            {
                for (int i = 13; i < width - 100; i += 96)
                {
                    g.DrawLine(pen_dot, i, startPosY + nCount * interval + 3, i, startPosY + nCount * interval + 3 + short_line_length);
                    g.DrawLine(pen_dot, i + n_X1_interval * 1, startPosY + nCount * interval + 3, i + n_X1_interval * 1, startPosY + nCount * interval + 3 + long_line_length);
                    g.DrawLine(pen_dot, i + n_X1_interval * 2, startPosY + nCount * interval + 3, i + n_X1_interval * 2, startPosY + nCount * interval + 3 + short_line_length);
                }
            }


            // 梯形线
            nCount = 10;
            g.DrawLine(pen, 0, startPosY + nCount * interval - 50, width - 1, startPosY + nCount * interval - 50);
            int tixingxian_height = interval * 24 / tixing_space;// 梯形块占用多少个小宽度，再除以8得高度
            for (int i = 0; i < width; i += tixing_space)
            {
                for (int j = 0; j < tixing_space; j++)
                {
                    g.DrawLine(pen_dot, i + j, startPosY + nCount * interval + tixingxian_height * j + 1, i + j, startPosY + nCount * interval + tixingxian_height * (j + 1));
                }
            }

            // 横直线 宽分别为1 2 3 4像素
            nCount = 36;
            int space = 15;
            // 宽度为1
            int line_offset = 10;// 直线相对于一整个interval的偏移
            int line_width = 1;
            for (int i = 0; i < line_width; i++)
            {
                g.DrawLine(pen, 0, startPosY + nCount * interval + line_offset + i, width - 1, startPosY + nCount * interval + line_offset + i);
            }
            //宽度为2 
            line_offset += space + line_width - 1;
            line_width = 2;
            for (int i = 0; i < line_width; i++)
            {
                g.DrawLine(pen, 0, startPosY + nCount * interval + line_offset + i, width - 1, startPosY + nCount * interval + line_offset + i);
            }
            //宽度为3
            line_offset += space + line_width - 1;
            line_width = 3;
            for (int i = 0; i < line_width; i++)
            {
                g.DrawLine(pen, 0, startPosY + nCount * interval + line_offset + i, width - 1, startPosY + nCount * interval + line_offset + i);
            }
            //宽度为4

            line_offset += space + line_width - 1;
            line_width = 4;
            for (int i = 0; i < line_width; i++)
            {
                g.DrawLine(pen, 0, startPosY + nCount * interval + line_offset + i, width - 1, startPosY + nCount * interval + line_offset + i);
            }

            // 横虚线
            int dashlength = 4;// 虚线白加黑的总长度
            int dashlength_black = 2;// 虚线黑线长度
            //宽度为1
            line_offset += space + line_width - 1;
            line_width = 1;
            for (int j = 0; j < line_width; j++)
            {
                for (int i = 0; i < width; i += dashlength)
                {
                    g.DrawLine(pen, i, startPosY + nCount * interval + line_offset + j, i + dashlength_black, startPosY + nCount * interval + line_offset + j);
                }
            }
            // 宽度为2
            line_offset += space + line_width - 1;
            line_width = 2;
            for (int j = 0; j < line_width; j++)
            {
                for (int i = 0; i < width; i += dashlength)
                {
                    g.DrawLine(pen, i, startPosY + nCount * interval + line_offset + j, i + dashlength_black, startPosY + nCount * interval + line_offset + j);
                }
            }
            // 宽度为1  增加需线条长度
            line_offset += space + line_width - 1;
            line_width = 1;
            dashlength = 12;
            dashlength_black = 5;
            for (int j = 0; j < line_width; j++)
            {
                for (int i = 0; i < width; i += dashlength)
                {
                    g.DrawLine(pen, i, startPosY + nCount * interval + line_offset + j, i + dashlength_black, startPosY + nCount * interval + line_offset + j);
                }
            }

            // 虚线宽度2
            line_offset += space + line_width - 1;
            line_width = 2;
            dashlength = 12;
            dashlength_black = 5;
            for (int j = 0; j < line_width; j++)
            {
                for (int i = 0; i < width; i += dashlength)
                {
                    g.DrawLine(pen, i, startPosY + nCount * interval + line_offset + j, i + dashlength_black, startPosY + nCount * interval + line_offset + j);
                }
            }


            nCount = 25;
            int color_block_interval = 4;// 色块线宽加间隔

            // 点阵
            nCount = 39;
            color_block_interval = 6;
            for (int i = 0; i < width; i += color_block_interval)
            {
                for (int j = startPosY + nCount * interval; j <= startPosY + nCount * interval + interval * 2; j += 6)
                {
                    g.FillRectangle(brush, i, j, 1, 1);
                }
                //g.DrawLine(pen_dot, i, startPosY + nCount * interval, i, startPosY + nCount * interval + interval * 2);
            }
            // 线条
            nCount = 41;
            color_block_interval = 4;
            for (int i = 0; i < width; i += color_block_interval)
            {
                g.DrawLine(pen_dot, i, startPosY + nCount * interval, i, startPosY + nCount * interval + interval * 2);
            }
            // 色块
            nCount = 43;
            color_block_interval = 1;
            for (int i = 0; i < width; i += color_block_interval)
            {
                g.DrawLine(pen, i, startPosY + nCount * interval, i, startPosY + nCount * interval + interval * 2);
            }

            g.DrawString(str_CMYK, font, Brushes.White, width*0.36f, startPosY + nCount * interval + 5);


            // 末尾分隔线
            for (int i = 1; i < 3; i++)
            {
                g.DrawLine(pen, 0, startPosY + height - i, width - 1, startPosY + height - i);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            pictureBox1.DrawToBitmap(bitmap, pictureBox1.ClientRectangle);
            // 32转1bit
            Bitmap newbitmap = bitmap.Clone(new Rectangle(0, 0, bitmap.Width, bitmap.Height), PixelFormat.Format1bppIndexed);
            bitmap.Dispose();
            newbitmap.Save($"output\\KJ4B_RC1200_2H\\penkong_{str_CMYK}_{str_checkPos}_1bit.bmp", ImageFormat.Bmp);


            newbitmap.Dispose();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    startPosY = 0 + deviation_angle_area_height;
                    str_CMYK = "M";
                    break;
                case 1:
                    startPosY = height / 4 * 1 + deviation_angle_area_height;
                    str_CMYK = "Y";

                    break;
                case 2:
                    startPosY = height / 4 * 2 + deviation_angle_area_height;
                    str_CMYK = "K";

                    break;
                case 3:
                    startPosY = height / 4 * 3 + deviation_angle_area_height;
                    str_CMYK = "C";

                    break;
                default:
                    startPosY = 0 + deviation_angle_area_height;
                    str_CMYK = "M";

                    break;
            }
            pictureBox1.Invalidate();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox2.SelectedIndex)
            {
                case 0:
                    startPosY_Angle = 0 + angle_area_interval / 2;
                    str_checkPos = "Base1";
                    m_bIsAngleBase = true;
                    break;
                case 1:
                    startPosY_Angle = deviation_angle_area_height / 2 * 1 + angle_area_interval / 2;
                    str_checkPos = "Base2";
                    m_bIsAngleBase = true;
                    break;
                case 2:
                    startPosY_Angle = 0 + angle_area_interval / 2;
                    str_checkPos = "Check1";
                    m_bIsAngleBase = false;
                    break;
                case 3:
                    startPosY_Angle = deviation_angle_area_height / 2 * 1 + angle_area_interval / 2;
                    str_checkPos = "Check2";
                    m_bIsAngleBase = false;
                    break;
                default:
                    startPosY_Angle = 0 + angle_area_interval / 2;
                    m_bIsAngleBase = true;
                    break;
            }
            pictureBox1.Invalidate();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int nResult = ModifyBMPBitsPerPixel(Application.StartupPath + $"output\\KJ4B_RC1200_2H\\penkong_{str_CMYK}_{str_checkPos}_1bit.bmp", Application.StartupPath + $"\\convert\\KJ4B_RC1200_2H\\penkong_{str_CMYK}_{str_checkPos}_1bit.bmp");

        }
    }
}
