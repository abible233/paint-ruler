﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintRuler
{
    public partial class FormNozzleRepairPage : Form
    {
        private const int width = 1600;
        private const int height = 2400;// 6000
        private const int count = 510;// 刻度数量
        private const int tick_interval = 4;// 刻度间隔
        private const int tick_length = 30;// 刻度长度
        private const int small_length_count = 8;// 一段阶梯有几个短线

        private int startPosY = 0;
        private int nHeadCnt = 20;

        private string str_CMYK = "C";
        private bool bRev = false;

        public FormNozzleRepairPage()
        {
            InitializeComponent();
        }
        private void DrawFunc(Graphics g)
        {
            g.Clear(Color.White);
            Pen pen = new Pen(Color.Black);
            SolidBrush brush = new SolidBrush(Color.Black);
            Font font = new Font("Arial", 156, FontStyle.Bold);
            Pen pen_dot = new Pen(Color.Black);
            pen_dot.DashStyle = DashStyle.Dot;



            int one_width = 3400 / 8;// 一个色块的宽度
            int first_offset = 225;// 第一个色块距离顶部的偏移
            int nOffset = 1;// 既作偏移，又作序号

            for (int n = 0; n < nHeadCnt; n++)
            {
                //if (comboBox1.SelectedIndex == 3)
                {
                    for (int y = 0; y < height * 4; y += height)
                    {
                        //g.FillRectangle(brush, width / 2 - 150, y + one_width / 2 + 80, 300, 200);
                        for (int y_1 = 0; y_1 < 200; y_1 += 2)
                        {
                            g.DrawLine(pen_dot, width * 0.3f + n * width, y + one_width / 2 + 80 + y_1, width * 0.3f + 300 + n * width, y + one_width / 2 + 80 + y_1);
                        }
                    }
                }



                g.DrawString(str_CMYK + $"{(bRev ? 20 - n : n + 1)}", font, Brushes.Black, width * 0.4f + n * width, startPosY + one_width / 2 + 80);


                // 刻度线
                nOffset = 1;
                for (int i = 0; i < count; i++)
                {
                    int j = 0;
                    for (int k = 0; k < small_length_count; k++)
                    {
                        g.DrawLine(pen, nOffset - 1 + i * tick_interval * small_length_count + tick_interval * j + n * width, nOffset * one_width + startPosY + first_offset + j * tick_length, nOffset - 1 + i * tick_interval * small_length_count + tick_interval * (j++) + n * width, nOffset * one_width + startPosY + first_offset + j * tick_length);
                    }
                }

                nOffset = 2;
                for (int i = 0; i < count; i++)
                {
                    int j = 0;
                    for (int k = 0; k < small_length_count; k++)
                    {
                        g.DrawLine(pen, nOffset - 1 + i * tick_interval * small_length_count + tick_interval * j + n * width, nOffset * one_width + startPosY + first_offset + j * tick_length, nOffset - 1 + i * tick_interval * small_length_count + tick_interval * (j++) + n * width, nOffset * one_width + startPosY + first_offset + j * tick_length);
                    }
                }

                nOffset = 3;
                for (int i = 0; i < count; i++)
                {
                    int j = 0;
                    for (int k = 0; k < small_length_count; k++)
                    {
                        g.DrawLine(pen, nOffset - 1 + i * tick_interval * small_length_count + tick_interval * j + n * width, nOffset * one_width + startPosY + first_offset + j * tick_length, nOffset - 1 + i * tick_interval * small_length_count + tick_interval * (j++) + n * width, nOffset * one_width + startPosY + first_offset + j * tick_length);
                    }
                }

                nOffset = 4;
                for (int i = 0; i < count; i++)
                {
                    int j = 0;
                    for (int k = 0; k < small_length_count; k++)
                    {
                        g.DrawLine(pen, nOffset - 1 + i * tick_interval * small_length_count + tick_interval * j + n * width, nOffset * one_width + startPosY + first_offset + j * tick_length, nOffset - 1 + i * tick_interval * small_length_count + tick_interval * (j++) + n * width, nOffset * one_width + startPosY + first_offset + j * tick_length);
                    }
                }
            }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            PictureBox picbox = (PictureBox)sender;
            Size size = new Size(width * nHeadCnt, height * 4);
            picbox.Size = size;

            Graphics g = e.Graphics;
            DrawFunc(g);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                DrawFunc(g);
            }

            // 32转1bit
            Bitmap newbitmap = bitmap.Clone(new Rectangle(0, 0, bitmap.Width, bitmap.Height), PixelFormat.Format1bppIndexed);

            newbitmap.Save($"output\\NozzleRepair\\penkongzhuangtai_wushuzi_{width}X{height}_1bit_{str_CMYK}_{nHeadCnt}Head{(bRev ? "_Rev" : "")}.bmp", ImageFormat.Bmp);

            bitmap.Dispose();
            newbitmap.Dispose();
        }
    }
}
