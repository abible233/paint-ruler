﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintRuler
{
    public partial class FormRectangle : Form
    {
        private int width = 50000;
        private int height = 904;

        private int Start_y = 0;
        private string str_cmyk = "c";
        int nWhite = 0;
        public FormRectangle()
        {
            InitializeComponent();
        }
        private void DrawFunc(Graphics g)
        {
            g.Clear(Color.White);
            Pen pen = new Pen(Color.Black);
            g.FillRectangle(Brushes.Black, nWhite, Start_y, width- nWhite*2, height);
            
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Size picture_size = new Size(width, height * 4);
            pictureBox1.Size = picture_size;
            //Font font = new Font("Arial", 90, FontStyle.Bold);

            Graphics g = e.Graphics;
            DrawFunc(g);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    Start_y = height * 0;
                    //nOffset = 0;
                    str_cmyk = "C";
                    break;
                case 1:
                    Start_y = height * 1;
                    //nOffset = 1;
                    str_cmyk = "M";
                    break;
                case 2:
                    Start_y = height * 2;
                    //nOffset = 2;
                    str_cmyk = "Y";
                    break;
                case 3:
                    Start_y = height * 3;
                    //nOffset = 3;
                    str_cmyk = "K";
                    break;
                default:
                    Start_y = 0;
                    //nOffset = 0;
                    str_cmyk = "C";
                    break;
            }
            pictureBox1.Invalidate();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                DrawFunc(g);
            }

            // 32转1bit
            Bitmap newbitmap = bitmap.Clone(new Rectangle(0, 0, bitmap.Width, bitmap.Height), PixelFormat.Format1bppIndexed);

            newbitmap.Save($"output\\Rectangle\\ceshitiao_{width}X{height * 4}_1bit_{str_cmyk}_{nWhite}.bmp", ImageFormat.Bmp);

            bitmap.Dispose();
            newbitmap.Dispose();
        }
    }
}
