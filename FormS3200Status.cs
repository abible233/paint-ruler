﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintRuler
{
    public partial class FormS3200Status : Form
    {
        private const int width = 2840;
        private const int height = 2200;// 3000pixels x 4

        private const int angle_area_interval = 46;// 角度两块之间的间隔 1mm
        private const int angle_base_length = 151;// 角度基准线
        private const int angle_check_length = 250;// 角度校准线
        private bool m_bIsAngleBase = true;// 当前是否是基准线

        private const int interval = 64;// 一组小线段所占的高度，用于和nCount相乘算Y位置
        private const int short_line_length = 80;// 长短线看夹的情况，短线长度
        private const int long_line_length = 150;// 长短线看夹的情况，长线长度
        private const int tixing_space = 8;// 梯形线的间隔


        private int startPosY = 0;
        private string str_CMYK = "C";
        public FormS3200Status()
        {
            InitializeComponent();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Size picture_size = new Size(width, height);
            pictureBox1.Size = picture_size;

            Graphics g = e.Graphics;
            DrawFunc(g);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                DrawFunc(g);
            }

            // 32转1bit
            Bitmap newbitmap = bitmap.Clone(new Rectangle(0, 0, bitmap.Width, bitmap.Height), PixelFormat.Format1bppIndexed);

            newbitmap.Save($"output\\S3200\\penkongzhuangtai_{width}X{height}_1bit.bmp", ImageFormat.Bmp);

            bitmap.Dispose();
            newbitmap.Dispose();
        }

        private void DrawFunc(Graphics g)
        {
            g.Clear(Color.White);
            Pen pen = new Pen(Color.Black);
            Font font = new Font("Arial", 90, FontStyle.Bold);
            Pen pen_dot = new Pen(Color.Black);
            pen_dot.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            SolidBrush brush = new SolidBrush(Color.Black);
            int nCount = 0;

            g.DrawLine(pen, 0, startPosY, width - 1, startPosY);
            // 短线夹长线
            for (int i = 0; i < width; i += 72)
            {
                if (width - i <= 18)
                    break;
                g.DrawLine(pen_dot, i, startPosY + nCount * interval + 3, i, startPosY + nCount * interval + 3 + short_line_length);
                g.DrawLine(pen_dot, i + 18, startPosY + nCount * interval + 3, i + 18, startPosY + nCount * interval + 3 + long_line_length);
                g.DrawLine(pen_dot, i + 36, startPosY + nCount * interval + 3, i + 36, startPosY + nCount * interval + 3 + short_line_length);
            }

            nCount = 3;
            for (int i = 0; i < width; i += 40)
            {
                g.DrawLine(pen_dot, i, startPosY + nCount * interval + 3, i, startPosY + nCount * interval + 3 + short_line_length);
                g.DrawLine(pen_dot, i + 10, startPosY + nCount * interval + 3, i + 10, startPosY + nCount * interval + 3 + long_line_length);
                g.DrawLine(pen_dot, i + 20, startPosY + nCount * interval + 3, i + 20, startPosY + nCount * interval + 3 + short_line_length);
            }

            nCount = 6;
            for (int i = 0; i < width; i += 24)
            {
                g.DrawLine(pen_dot, i, startPosY + nCount * interval + 3, i, startPosY + nCount * interval + 3 + short_line_length);
                g.DrawLine(pen_dot, i + 6, startPosY + nCount * interval + 3, i + 6, startPosY + nCount * interval + 3 + long_line_length);
                g.DrawLine(pen_dot, i + 12, startPosY + nCount * interval + 3, i + 12, startPosY + nCount * interval + 3 + short_line_length);
            }


            // 梯形线
            nCount = 10;
            g.DrawLine(pen, 0, startPosY + nCount * interval - 50, width - 1, startPosY + nCount * interval - 50);
            int tixingxian_height = interval * 12 / tixing_space;// 梯形块占用多少个小宽度，再除以8得高度
            for (int i = 0; i < width; i += tixing_space)
            {
                for (int j = 0; j < tixing_space; j++)
                {
                    g.DrawLine(pen_dot, i + j, startPosY + nCount * interval + tixingxian_height * j + 1, i + j, startPosY + nCount * interval + tixingxian_height * (j + 1));
                }
            }

            // 横直线 宽分别为1 2 3 4像素
            nCount = 23;
            int space = 15;
            // 宽度为1
            int line_offset = 10;// 直线相对于一整个interval的偏移
            int line_width = 1;
            for (int i = 0; i < line_width; i++)
            {
                g.DrawLine(pen, 0, startPosY + nCount * interval + line_offset + i, width - 1, startPosY + nCount * interval + line_offset + i);
            }
            //宽度为2 
            line_offset += space + line_width - 1;
            line_width = 2;
            for (int i = 0; i < line_width; i++)
            {
                g.DrawLine(pen, 0, startPosY + nCount * interval + line_offset + i, width - 1, startPosY + nCount * interval + line_offset + i);
            }
            //宽度为3
            line_offset += space + line_width - 1;
            line_width = 3;
            for (int i = 0; i < line_width; i++)
            {
                g.DrawLine(pen, 0, startPosY + nCount * interval + line_offset + i, width - 1, startPosY + nCount * interval + line_offset + i);
            }
            //宽度为4

            line_offset += space + line_width - 1;
            line_width = 4;
            for (int i = 0; i < line_width; i++)
            {
                g.DrawLine(pen, 0, startPosY + nCount * interval + line_offset + i, width - 1, startPosY + nCount * interval + line_offset + i);
            }

            // 横虚线
            int dashlength = 4;// 虚线白加黑的总长度
            int dashlength_black = 2;// 虚线黑线长度
            //宽度为1
            line_offset += space + line_width - 1;
            line_width = 1;
            for (int j = 0; j < line_width; j++)
            {
                for (int i = 0; i < width; i += dashlength)
                {
                    g.DrawLine(pen, i, startPosY + nCount * interval + line_offset + j, i + dashlength_black, startPosY + nCount * interval + line_offset + j);
                }
            }
            // 宽度为2
            line_offset += space + line_width - 1;
            line_width = 2;
            for (int j = 0; j < line_width; j++)
            {
                for (int i = 0; i < width; i += dashlength)
                {
                    g.DrawLine(pen, i, startPosY + nCount * interval + line_offset + j, i + dashlength_black, startPosY + nCount * interval + line_offset + j);
                }
            }
            // 宽度为1  增加需线条长度
            line_offset += space + line_width - 1;
            line_width = 1;
            dashlength = 12;
            dashlength_black = 5;
            for (int j = 0; j < line_width; j++)
            {
                for (int i = 0; i < width; i += dashlength)
                {
                    g.DrawLine(pen, i, startPosY + nCount * interval + line_offset + j, i + dashlength_black, startPosY + nCount * interval + line_offset + j);
                }
            }

            // 虚线宽度2
            line_offset += space + line_width - 1;
            line_width = 2;
            dashlength = 12;
            dashlength_black = 5;
            for (int j = 0; j < line_width; j++)
            {
                for (int i = 0; i < width; i += dashlength)
                {
                    g.DrawLine(pen, i, startPosY + nCount * interval + line_offset + j, i + dashlength_black, startPosY + nCount * interval + line_offset + j);
                }
            }


            nCount = 25;
            int color_block_interval = 4;// 色块线宽加间隔

            nCount = 27;
            color_block_interval = 6;
            for (int i = 0; i < width; i += color_block_interval)
            {
                for (int j = startPosY + nCount * interval; j <= startPosY + nCount * interval + interval * 2; j += 6)
                {
                    g.FillRectangle(brush, i, j, 1, 1);
                }

                //g.DrawLine(pen_dot, i, startPosY + nCount * interval, i, startPosY + nCount * interval + interval * 2);
            }
            nCount = 29;
            color_block_interval = 4;
            for (int i = 0; i < width; i += color_block_interval)
            {
                g.DrawLine(pen_dot, i, startPosY + nCount * interval, i, startPosY + nCount * interval + interval * 2);
            }
            nCount = 31;
            color_block_interval = 1;
            for (int i = 0; i < width; i += color_block_interval)
            {
                g.DrawLine(pen, i, startPosY + nCount * interval, i, startPosY + nCount * interval + interval * 2);
            }

            int str_x_pos = 320;
            g.DrawString(str_CMYK, font, Brushes.White, str_x_pos, startPosY + nCount * interval + 5);
            g.DrawString(str_CMYK, font, Brushes.White, str_x_pos + 710 * 1, startPosY + nCount * interval + 5);
            g.DrawString(str_CMYK, font, Brushes.White, str_x_pos + 710 * 2, startPosY + nCount * interval + 5);
            g.DrawString(str_CMYK, font, Brushes.White, str_x_pos + 710 * 3, startPosY + nCount * interval + 5);


            // 末尾分隔线
            for (int i = 1; i < 3; i++)
            {
                g.DrawLine(pen, 0, startPosY + height - i, width - 1, startPosY + height - i);
            }
        }
    }
}
