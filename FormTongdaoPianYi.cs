﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintRuler
{
    public partial class FormTongdaoPianYi : Form
    {
        public FormTongdaoPianYi()
        {
            InitializeComponent();
        }

        private const int Width = 800;
        private const int Height = 800;

        private bool isBase = true;
        private int StartPosY = 0;
        private string str_CMYK = "Base";

        private int YOffset = 100;
        private int YFontOffset = 50;
        private int LineLength = 500;

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            PictureBox picbox = (PictureBox)sender;
            Size size = new Size(Width, Height * 3);
            picbox.Size = size;

            Graphics g = e.Graphics;
            g.Clear(Color.White);
            Pen pen = new Pen(Color.Black);
            Font font = new Font("Arial", 36, FontStyle.Regular);

            int StartPosX = Width / 2;

            if (isBase)
            {
                for(int i = 0; i < 3; i++)
                {
                    StartPosY = Height * i;

                    for(int j = 0; j < 8; j++)
                    {
                        if (j == 0)
                        {
                            g.DrawLine(pen, StartPosX, StartPosY + YOffset + YFontOffset, StartPosX, StartPosY + YFontOffset + YOffset + LineLength);
                            g.DrawString("0", font, Brushes.Black, new PointF(StartPosX-21, StartPosY + YOffset));
                        }
                        else
                        {

                            g.DrawLine(pen, StartPosX + j * 50, StartPosY + YOffset + YFontOffset, StartPosX + j * 50, StartPosY + YFontOffset + YOffset + LineLength);
                            if (j < 10)
                                g.DrawString(j.ToString(), font, Brushes.Black, new PointF(StartPosX + j * 50 - 21, StartPosY + YOffset));
                            else if (j % 2 == 0)
                            {
                                g.DrawString(j.ToString(), font, Brushes.Black, new PointF(StartPosX + j * 50 - 35, StartPosY + YOffset));
                            }

                            g.DrawLine(pen, StartPosX - j * 50, StartPosY + YOffset + YFontOffset, StartPosX - j * 50, StartPosY + YFontOffset + YOffset + LineLength);
                            if (j < 10)
                                g.DrawString((-j).ToString(), font, Brushes.Black, new PointF(StartPosX - j * 50 - 30, StartPosY + YOffset));
                            else if (j % 2 == 0)
                            {
                                g.DrawString((-j).ToString(), font, Brushes.Black, new PointF(StartPosX - j * 50 - 50, StartPosY + YOffset));
                            }
                        }
                    }
                }
            }
            else
            {
                for (int j = 0; j < 40; j++)
                {
                    if (j == 0)
                    {
                        g.DrawLine(pen, StartPosX, StartPosY + YOffset + YFontOffset, StartPosX, StartPosY + YFontOffset + YOffset + LineLength);
                        //g.DrawString("0", font, Brushes.Black, new PointF(StartPosX, StartPosY + YOffset));
                    }
                    else
                    {
                        g.DrawLine(pen, StartPosX + j * 51, StartPosY + YOffset + YFontOffset, StartPosX + j * 51, StartPosY + YFontOffset + YOffset + LineLength);
                        //g.DrawString(j.ToString(), font, Brushes.Black, new PointF(StartPosX + j * 51, StartPosY + YOffset));

                        g.DrawLine(pen, StartPosX - j * 51, StartPosY + YOffset + YFontOffset, StartPosX - j * 51, StartPosY + YFontOffset + YOffset + LineLength);
                        //g.DrawString((-j).ToString(), font, Brushes.Black, new PointF(StartPosX - j * 51, StartPosY + YOffset));
                    }
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    isBase = false;
                    StartPosY = 0;
                    str_CMYK = "0";
                    break;
                case 1:
                    isBase = false;
                    StartPosY = Height * 1;
                    str_CMYK = "1";

                    break;
                case 2:
                    isBase = false;
                    StartPosY = Height * 2;
                    str_CMYK = "2";
                    break;
                case 3:
                    isBase = true;
                    StartPosY = 0;
                    str_CMYK = "Base";

                    break;
                default:
                    isBase = true;
                    StartPosY = 0;
                    str_CMYK = "Base";

                    break;
            }
            pictureBox1.Invalidate();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            pictureBox1.DrawToBitmap(bitmap, pictureBox1.ClientRectangle);
            // 32转1bit
            Bitmap newbitmap = bitmap.Clone(new Rectangle(0, 0, bitmap.Width, bitmap.Height), PixelFormat.Format1bppIndexed);

            //bitmap.Save("output\\scale_ruler_32bit.bmp", ImageFormat.Bmp);
            if (isBase)
                newbitmap.Save($"output\\I3200-4color-800\\TongdaoPianyi_{Width}X{Height * 3}_{str_CMYK}.bmp", ImageFormat.Bmp);
            else
                newbitmap.Save($"output\\I3200-4color-800\\TongdaoPianyi_{Width}X{Height * 3}_{str_CMYK}.bmp", ImageFormat.Bmp);

            bitmap.Dispose();
            newbitmap.Dispose();
        }
    }
}
