﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintRuler
{
    public partial class Formpenkongbuchangrengong : Form
    {
        private const int width = 800;
        private const int height = 6000;
        private const int ColorBarHeight = 170;// 色条高度

        private const int RulerNum = 16;// 刻度尺数量
        private const float RulerHeight = 330;// 刻度尺高度
        private const int ScaleNum = 318;// 刻度尺拥有多少个小格子


        private float ScaleLength;// 刻度尺小格子长度

        private int startPosY = 0;
        private string str_CMYK = "C";
        public Formpenkongbuchangrengong()
        {
            InitializeComponent();
            ScaleLength = 1 * RulerNum;// 几把尺子代表一小格占多少像素
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            PictureBox picbox = (PictureBox)sender;
            Size size = new Size(width, height*4);
            picbox.Size = size;

            Graphics g = e.Graphics;
            g.Clear(Color.White);
            Pen pen = new Pen(Color.Black);
            SolidBrush brush = new SolidBrush(Color.Black);
            Font font = new Font("Arial", 26, FontStyle.Regular);
            Font font_CMYK = new Font("Arial", 152, FontStyle.Bold);
            Font font_EG = new Font("Arial", 44, FontStyle.Bold);
            Pen pen_dot = new Pen(Color.Black);
            pen_dot.DashStyle = DashStyle.Dot;



            int first_offset = 100;// 留白
            float ColorBarPos = first_offset + RulerHeight * RulerNum + first_offset;// 一个色条的起始位置
            int nLineOffset = 10;// 既作偏移，又作序号

            // 顶部线
            g.DrawLine(pen, 0, startPosY + nLineOffset, width - 1, startPosY + nLineOffset);

            // 顶部注释
            g.DrawString($"每小格间隔{ScaleLength}个孔", font_EG, Brushes.Black, width- 600,startPosY+25);

            // 色块
            for (int m=0; m<3;m++)
            {
                if (m == 0)
                {
                    for (int i = 0; i < width; i += 5)
                    {
                        for (float j = startPosY + ColorBarPos + ColorBarHeight * m; j <= startPosY + ColorBarPos + ColorBarHeight * (m + 1); j += 5)
                        {
                            g.FillRectangle(brush, i, j, 1, 1);
                        }
                        //g.DrawLine(pen_dot, i, startPosY + ColorBarPos + ColorBarHeight * m, i, startPosY + ColorBarPos + ColorBarHeight * (m + 1));
                    }
                }
                if (m == 1)
                {
                    for (int i = 0; i < width; i += 3)
                    {
                        g.DrawLine(pen, i, startPosY + ColorBarPos + ColorBarHeight * m, i, startPosY + ColorBarPos + ColorBarHeight * (m + 1));
                    }
                }
                if (m == 2)
                {
                    for (int i = 0; i < width; i++)
                    {
                        g.DrawLine(pen, i, startPosY + ColorBarPos + ColorBarHeight * m, i, startPosY + ColorBarPos + ColorBarHeight * (m + 1));
                    }
                }
            }

            g.DrawString(str_CMYK, font_CMYK, Brushes.White, width*0.36f, startPosY + ColorBarPos + ColorBarHeight * 2 - 25);


            // 尺子顶部线
            g.DrawLine(pen, 0, startPosY + first_offset , width - 1, startPosY + first_offset);
            for (int i = 0; i < 16; i++)
            {
                float y = first_offset + i * RulerHeight;
                g.DrawLine(pen, 0, startPosY + y + RulerHeight, width - 1, startPosY + y + RulerHeight);// 刻度尺下沿
                g.DrawLine(pen, 0, startPosY + y + RulerHeight / 3, width - 1, startPosY + y + RulerHeight / 3);// 刻度尺中沿

                float x = 0;
                int j = 0;
                while (x < width)
                {
                    x = i + j * ScaleLength;
                    if (j % 5 == 0)
                    {
                        g.DrawLine(pen, x, startPosY + y, x, startPosY + y + RulerHeight - 1);
                        if (j + 2 < ScaleNum)
                        {
                            if (j % 10 == 0)
                            {
                                    g.DrawString((i + j * 16 + 1).ToString(), font, Brushes.Black, x - 7, startPosY + y + 10);
                            }
                            else
                            {
                                if (j + 2 > ScaleNum - 5)
                                {
                                    //if (i > 4&&i<8)
                                    //    g.DrawString((i + j * 16 + 1).ToString(), font, Brushes.Black, x - 12, startPosY + y + 60);
                                    //else if(i>=8)
                                    //    g.DrawString((i + j * 16 + 1).ToString(), font, Brushes.Black, x - 20, startPosY + y + 60);
                                    //else
                                    //    g.DrawString((i + j * 16 + 1).ToString(), font, Brushes.Black, x - 7, startPosY + y + 60);
                                    g.DrawString((i + j * 16 + 1).ToString(), font, Brushes.Black, x - 5 - i, startPosY + y + 60);
                                }
                                else
                                    g.DrawString((i + j * 16 + 1).ToString(), font, Brushes.Black, x - 7, startPosY + y + 60);
                            }
                        }
                    }
                    else
                        g.DrawLine(pen, x, startPosY + y + RulerHeight, x, startPosY + y + RulerHeight / 3);
                    j++;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            pictureBox1.DrawToBitmap(bitmap, pictureBox1.ClientRectangle);
            // 32转1bit
            Bitmap newbitmap = bitmap.Clone(new Rectangle(0, 0, bitmap.Width, bitmap.Height), PixelFormat.Format1bppIndexed);

            newbitmap.Save($"output\\I3200-4color-800\\buchangrengong_{width}X{height}_1bit_{str_CMYK}.bmp", ImageFormat.Bmp);

            bitmap.Dispose();
            newbitmap.Dispose();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    startPosY = 0;
                    //nOffset = 0;
                    str_CMYK = "C";
                    break;
                case 1:
                    startPosY = height * 1;
                    //nOffset = 1;
                    str_CMYK = "M";
                    break;
                case 2:
                    startPosY = height * 2;
                    //nOffset = 2;
                    str_CMYK = "Y";
                    break;
                case 3:
                    startPosY = height * 3;
                    //nOffset = 3;
                    str_CMYK = "K";
                    break;
                default:
                    startPosY = 0;
                    //nOffset = 0;
                    str_CMYK = "C";
                    break;
            }
            pictureBox1.Invalidate();
        }
    }
}
