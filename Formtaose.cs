﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintRuler
{
    public partial class Formtaose : Form
    {
        private const int width = 5116;
        private const int height = 4500;
        private const int ruler_length = 4300;
        private const int lines_space = 0;// 刻度基本线间隔
        private const int mark_line_length = 200;// 刻度长度
        private const int count = 21;// 刻度数量
        private const int tick_interval = 100;// 刻度间隔
        private const int tick_interval_tocheck = 101;// 第二个刻度间隔，两者不同，用于校准

        private int startPosY = 0;
        private string str_CMYK = "C";
        private bool m_bIsBase;

        public Formtaose()
        {
            InitializeComponent();
            m_bIsBase = true;
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            PictureBox picbox = (PictureBox)sender;
            Size size = new Size(width, height*3);
            picbox.Size = size;

            Graphics g = e.Graphics;
            g.Clear(Color.White);
            Pen pen = new Pen(Color.Black);
            SolidBrush brush = new SolidBrush(Color.Black);
            Font font = new Font("Arial", 36, FontStyle.Regular);
            Matrix matrix = new Matrix();

            if(m_bIsBase)
            {
                for (int j = 0; j < 3; j++)
                {
                    int startPosY_1 = height * j;
                    // 基本线
                    //g.DrawLine(pen, (width - lines_space) / 2, startPosY_1 + (height - ruler_length) / 2, (width - lines_space) / 2, startPosY_1 + (height + ruler_length) / 2);
                    //g.DrawLine(pen, (width + lines_space) / 2, startPosY_1 + (height - ruler_length) / 2, (width + lines_space) / 2, startPosY_1 + (height + ruler_length) / 2);


                    // 0刻度线
                    g.DrawLine(pen, (width - lines_space) / 2, startPosY_1 + height / 2, (width - lines_space) / 2 - mark_line_length - 20, startPosY_1 + height / 2); // 0长一点  20
                    g.DrawLine(pen, (width - lines_space) / 2 - 80, startPosY_1 + height / 2 + 30, (width - lines_space) / 2-50 , startPosY_1 + height / 2);
                    g.DrawLine(pen, (width - lines_space) / 2 - 80, startPosY_1 + height / 2 - 30, (width - lines_space) / 2 - 50, startPosY_1 + height / 2);

                    g.FillRectangle(brush, (width - lines_space) / 2 - 170, startPosY_1 + height / 2 - 15, 30, 30);

                    // 创建一个新的旋转变换矩阵
                    matrix = new Matrix();
                    matrix.RotateAt(270, new PointF((width - lines_space) / 2 - mark_line_length, startPosY_1 + height / 2));

                    // 应用旋转变换
                    g.Transform = matrix;

                    // 绘制旋转后的文字
                    g.DrawString("0", font, Brushes.Black, new PointF((width - lines_space) / 2 - mark_line_length - 35, startPosY_1 + height / 2 - 70));

                    // 恢复默认的变换
                    g.ResetTransform();
                    for (int i = 1; i < count; i++)
                    {
                        g.DrawLine(pen, (width - lines_space) / 2 - mark_line_length, startPosY_1 + height / 2 + i * tick_interval, (width - lines_space) / 2, startPosY_1 + height / 2 + i * tick_interval);
                        g.DrawLine(pen, (width - lines_space) / 2 - mark_line_length, startPosY_1 + height / 2 - i * tick_interval, (width - lines_space) / 2, startPosY_1 + height / 2 - i * tick_interval);
                        if (i < 10 && i % 2 == 0)
                        {
                            matrix = new Matrix();
                            matrix.RotateAt(270, new PointF((width - lines_space) / 2 - mark_line_length, startPosY_1 + height / 2 + i * tick_interval));
                            g.Transform = matrix;

                            g.DrawString((-i).ToString(), font, Brushes.Black, (width - lines_space) / 2 - mark_line_length - 55, startPosY_1 + height / 2 + i * tick_interval - 50);
                            g.ResetTransform();

                            matrix = new Matrix();
                            matrix.RotateAt(270, new PointF((width - lines_space) / 2 - mark_line_length, startPosY_1 + height / 2 - i * tick_interval));
                            g.Transform = matrix;

                            g.DrawString((i).ToString(), font, Brushes.Black, (width - lines_space) / 2 - mark_line_length - 40, startPosY_1 + height / 2 - i * tick_interval - 50);
                            g.ResetTransform();
                        }
                        if (i >= 10 && i < 100 && i % 2 == 0)
                        {
                            matrix = new Matrix();
                            matrix.RotateAt(270, new PointF((width - lines_space) / 2 - mark_line_length, startPosY_1 + height / 2 + i * tick_interval));
                            g.Transform = matrix;

                            g.DrawString((-i).ToString(), font, Brushes.Black, (width - lines_space) / 2 - mark_line_length - 75, startPosY_1 + height / 2 + i * tick_interval - 50);
                            g.ResetTransform();

                            matrix = new Matrix();
                            matrix.RotateAt(270, new PointF((width - lines_space) / 2 - mark_line_length, startPosY_1 + height / 2 - i * tick_interval));
                            g.Transform = matrix;

                            g.DrawString((i).ToString(), font, Brushes.Black, (width - lines_space) / 2 - mark_line_length - 60, startPosY_1 + height / 2 - i * tick_interval - 50);
                            g.ResetTransform();
                        }
                    }

                    // 色块
                    for(int i = 0; i < count * 2 - 1; i++)
                    {
                        g.FillRectangle(brush, (width - lines_space) / 2 - mark_line_length, startPosY_1 + height / 2 - (count - 1) * tick_interval + i * tick_interval + 30, mark_line_length * 3, 30);
                        //g.DrawRectangle(pen, (width - lines_space) / 2 - mark_line_length, startPosY_1 + height / 2 - (count-1) * tick_interval + i*tick_interval,mark_line_length*2,40);
                    }
                }
            }
            else
            {
                for (int j = 0; j < 1; j++)
                {
                    //g.DrawLine(pen, (width + lines_space) / 2, startPosY + (height - ruler_length) / 2, (width + lines_space) / 2, startPosY + (height + ruler_length) / 2);
                    g.DrawLine(pen, (width + lines_space) / 2, startPosY + height / 2, (width + lines_space) / 2 + mark_line_length + 20, startPosY + height / 2);// 0刻度线长一点  20

                    matrix = new Matrix();
                    matrix.RotateAt(90, new PointF((width + lines_space) / 2 + mark_line_length, startPosY + height / 2));
                    g.Transform = matrix;
                    g.DrawString("0", font, Brushes.Black, (width + lines_space) / 2 + mark_line_length - 1, startPosY + height / 2 - 70);
                    g.ResetTransform();
                    for (int i = 1; i < count; i++)
                    {
                        g.DrawLine(pen, (width + lines_space) / 2 + mark_line_length, startPosY + height / 2 + i * tick_interval_tocheck, (width + lines_space) / 2, startPosY + height / 2 + i * tick_interval_tocheck);
                        g.DrawLine(pen, (width + lines_space) / 2 + mark_line_length, startPosY + height / 2 - i * tick_interval_tocheck, (width + lines_space) / 2, startPosY + height / 2 - i * tick_interval_tocheck);
                        if (i < 10 && i % 2 == 0)
                        {
                            matrix = new Matrix();
                            matrix.RotateAt(90, new PointF((width + lines_space) / 2 + mark_line_length, startPosY + height / 2 + i * tick_interval_tocheck));
                            g.Transform = matrix;
                            g.DrawString((-i).ToString(), font, Brushes.Black, (width + lines_space) / 2 + mark_line_length -10, startPosY + height / 2 + i * tick_interval_tocheck - 50);
                            g.ResetTransform();

                            matrix = new Matrix();
                            matrix.RotateAt(90, new PointF((width + lines_space) / 2 + mark_line_length, startPosY + height / 2 - i * tick_interval_tocheck));
                            g.Transform = matrix;
                            g.DrawString((i).ToString(), font, Brushes.Black, (width + lines_space) / 2 + mark_line_length , startPosY + height / 2 - i * tick_interval_tocheck - 50);
                            g.ResetTransform();
                        }
                        if (i >= 10 && i < 100 && i % 2 == 0)
                        {
                            matrix = new Matrix();
                            matrix.RotateAt(90, new PointF((width + lines_space) / 2 + mark_line_length, startPosY + height / 2 + i * tick_interval_tocheck));
                            g.Transform = matrix;
                            g.DrawString((-i).ToString(), font, Brushes.Black, (width + lines_space) / 2 + mark_line_length - 20, startPosY + height / 2 + i * tick_interval_tocheck - 50);
                            g.ResetTransform();

                            matrix = new Matrix();
                            matrix.RotateAt(90, new PointF((width + lines_space) / 2 + mark_line_length, startPosY + height / 2 - i * tick_interval_tocheck));
                            g.Transform = matrix;
                            g.DrawString((i).ToString(), font, Brushes.Black, (width + lines_space) / 2 + mark_line_length - 15 , startPosY + height / 2 - i * tick_interval_tocheck - 50);
                            g.ResetTransform();
                        }
                    }

                    // 色块
                    for (int i = 0; i < count * 2 - 1; i++)
                    {
                        g.FillRectangle(brush, (width - lines_space) / 2, startPosY + height / 2 - (count - 1) * tick_interval_tocheck + i * tick_interval_tocheck + 30, mark_line_length, 30);
                        //g.DrawRectangle(pen, (width - lines_space) / 2 - mark_line_length, startPosY_1 + height / 2 - (count-1) * tick_interval + i*tick_interval,mark_line_length*2,40);
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            pictureBox1.DrawToBitmap(bitmap, pictureBox1.ClientRectangle);
            // 32转1bit
            Bitmap newbitmap = bitmap.Clone(new Rectangle(0, 0, bitmap.Width, bitmap.Height), PixelFormat.Format1bppIndexed);

            //bitmap.Save("output\\scale_ruler_32bit.bmp", ImageFormat.Bmp);
            if(m_bIsBase)
                newbitmap.Save($"output\\KJ4B_RC1200_2H\\4color\\neibutaose_{width}X{height}_1bit_Base.bmp", ImageFormat.Bmp);
            else
                newbitmap.Save($"output\\KJ4B_RC1200_2H\\4color\\neibutaose_{width}X{height}_1bit_{str_CMYK}.bmp", ImageFormat.Bmp);

            bitmap.Dispose();
            newbitmap.Dispose();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    startPosY = 0;
                    str_CMYK = "C";
                    break;
                case 1:
                    startPosY = height * 1;
                    str_CMYK = "M";

                    break;
                case 2:
                    startPosY = height * 2;
                    str_CMYK = "Y";

                    break;
                case 3:
                    startPosY = height * 3;
                    str_CMYK = "K";

                    break;
                default:
                    startPosY = 0;
                    str_CMYK = "C";

                    break;
            }
            pictureBox1.Invalidate();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            m_bIsBase = checkBox1.Checked;
            pictureBox1.Invalidate();
        }
    }
}
