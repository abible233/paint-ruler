﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintRuler
{
    public partial class Formtaose2 : Form
    {
        private const int width = 5116;
        private const int height = 2400;//标准2400  检测用6000
        private const int ruler_length = 2000;
        private const int mark_line_length = 150;// 刻度长度
        private const int count = 10;// 刻度数量
        private const int tick_interval = 100;// 刻度间隔
        private const int tick_interval_tocheck = 101;// 第二个刻度间隔，两者不同，用于校准

        private int startPosY = 0;
        private string str_CMYK = "K";
        public Formtaose2()
        {
            InitializeComponent();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            PictureBox picbox = (PictureBox)sender;
            Size size = new Size(width, height);
            picbox.Size = size;

            Graphics g = e.Graphics;
            g.Clear(Color.White);
            Pen pen = new Pen(Color.Black);
            Pen pen_dot = new Pen(Color.Black);
            pen_dot.DashStyle = DashStyle.Dot;
            SolidBrush brush = new SolidBrush(Color.Black);
            Font font = new Font("Arial", 36, FontStyle.Regular);
            Matrix matrix = new Matrix();
            Font font_1 = new Font("Arial", 80, FontStyle.Bold);

            // 水平线
            //g.DrawLine(pen, 0, startPosY + (height - ruler_length) / 3, width - 1, startPosY + (height - ruler_length) / 3);
            //g.DrawLine(pen, 0, startPosY + (height - ruler_length) / 3+1, width - 1, startPosY + (height - ruler_length) / 3+1);
            for(int i = 0; i < 80; i++)
            {
                g.DrawLine(pen, 0, startPosY + (height - ruler_length) / 3 + i, width - 1, startPosY + (height - ruler_length) / 3 + i);
            }
            g.DrawString(str_CMYK, font_1, Brushes.White, width *0.43f, startPosY + (height - ruler_length) / 3 - 19);

            // CMKY 在中间
            //g.DrawString(str_CMYK, font_1, Brushes.Black, width / 2 - 500, startPosY + height  / 2 -200);

            // 黑色色块
            //if(comboBox1.SelectedIndex==3)
            //{
            //    // 画四个
            //    for(int m =0;m<4;m++)
            //    {
            //        for (int i = 0; i < 200; i += 2)
            //        {
            //            g.DrawLine(pen_dot, width / 2 - 150, height*m + height / 2 -200 + i, width / 2 + 150, height*m + height  / 2 -200 + i);
            //        }
            //    }
            //}

            // 基本线
            //g.DrawLine(pen, 0, startPosY + (height - ruler_length) / 2, 0, startPosY + (height + ruler_length) / 2);
            //g.DrawLine(pen, width - 1, startPosY + (height - ruler_length) / 2, width - 1, startPosY + (height + ruler_length) / 2);

            // 刻度线
            g.DrawLine(pen, 0, startPosY + height / 2, mark_line_length  , startPosY + height / 2);
            g.DrawLine(pen, 40, startPosY + height / 2 + 60, 100, startPosY + height / 2);
            g.DrawLine(pen, 40, startPosY + height / 2 - 60, 100, startPosY + height / 2);
            g.FillRectangle(brush, 100, startPosY + height / 2 - 20, 40, 40);

            matrix = new Matrix();
            matrix.RotateAt(270, new PointF(mark_line_length , startPosY + height / 2 ));

            // 应用旋转变换
            g.Transform = matrix;
            g.DrawString("0", font, Brushes.Black, mark_line_length - 35, startPosY + height / 2 +10);
            g.ResetTransform();
            for (int i = 1; i < count; i++)
            {
                g.DrawLine(pen, 0, startPosY + height / 2 + i * tick_interval, mark_line_length, startPosY + height / 2 + i * tick_interval);
                g.DrawLine(pen, 0, startPosY + height / 2 - i * tick_interval, mark_line_length, startPosY + height / 2 - i * tick_interval);

                if(i%2==0)
                {
                    matrix = new Matrix();
                    matrix.RotateAt(90, new PointF(mark_line_length, startPosY + height / 2 + i * tick_interval));

                    // 应用旋转变换
                    g.Transform = matrix;
                    g.DrawString((i).ToString(), font, Brushes.Black, mark_line_length , startPosY + height / 2 + i * tick_interval - 65);
                    g.ResetTransform();

                    matrix = new Matrix();
                    matrix.RotateAt(90, new PointF(mark_line_length, startPosY + height / 2 - i * tick_interval));

                    // 应用旋转变换
                    g.Transform = matrix;
                    g.DrawString((-i).ToString(), font, Brushes.Black, mark_line_length -5, startPosY + height / 2 - i * tick_interval -65);
                    g.ResetTransform();
                }
            }

            g.DrawLine(pen, width - 1, startPosY + height / 2, width - 1 - mark_line_length , startPosY + height / 2);
            g.DrawLine(pen, 0, startPosY + height / 2, mark_line_length, startPosY + height / 2);
            g.DrawLine(pen, width-1-mark_line_length+40, startPosY + height / 2 + 60, width - 1 - mark_line_length+100, startPosY + height / 2);
            g.DrawLine(pen, width - 1 - mark_line_length + 40, startPosY + height / 2 - 60, width - 1 - mark_line_length + 100, startPosY + height / 2);
            matrix = new Matrix();
            matrix.RotateAt(270, new PointF(width - 1 - mark_line_length, startPosY + height / 2));

            // 应用旋转变换
            g.Transform = matrix;
            g.DrawString("0", font, Brushes.Black, width - 1 - mark_line_length - 35, startPosY + height / 2 -50);
            g.ResetTransform();
            for (int i = 1; i < count; i++)
            {
                g.DrawLine(pen, width - 1, startPosY + height / 2 + i * tick_interval_tocheck, width - 1 - mark_line_length, startPosY + height / 2 + i * tick_interval_tocheck);
                g.DrawLine(pen, width - 1, startPosY + height / 2 - i * tick_interval_tocheck, width - 1 - mark_line_length, startPosY + height / 2 - i * tick_interval_tocheck);

                if(i%2==0)
                {
                    matrix = new Matrix();
                    matrix.RotateAt(270, new PointF(width - 1 - mark_line_length, startPosY + height / 2 + i * tick_interval_tocheck));

                    // 应用旋转变换
                    g.Transform = matrix;
                    g.DrawString((i).ToString(), font, Brushes.Black, width - 1 - mark_line_length - 40, startPosY + height / 2 + i * tick_interval_tocheck - 50);
                    g.ResetTransform();


                    matrix = new Matrix();
                    matrix.RotateAt(270, new PointF(width - 1 - mark_line_length, startPosY + height / 2 - i * tick_interval_tocheck));

                    // 应用旋转变换
                    g.Transform = matrix;
                    g.DrawString((-i).ToString(), font, Brushes.Black, width - 1 - mark_line_length - 55, startPosY + height / 2 - i * tick_interval_tocheck - 50);
                    g.ResetTransform();
                }
            }

            // 水平线
            for (int i = 0; i < 80; i++)
            {
                g.DrawLine(pen, 0, startPosY + ruler_length + (height - ruler_length) * 2 / 3 + i, width - 1, startPosY + ruler_length + (height - ruler_length) * 2 / 3 + i);
            }
            g.DrawString(str_CMYK, font_1, Brushes.White, width * 0.48f, startPosY + ruler_length + (height - ruler_length) * 2 / 3 - 19);


            //色块
            for (int i = 0; i < count * 2 - 1; i++)
            {
                g.FillRectangle(brush, 0, startPosY + height / 2 - (count-1) * tick_interval + i * tick_interval+ 30, mark_line_length, 30);
                g.FillRectangle(brush, width-1-mark_line_length, startPosY + height / 2 - (count - 1) * tick_interval_tocheck + i * tick_interval_tocheck + 30, mark_line_length, 30);

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            pictureBox1.DrawToBitmap(bitmap, pictureBox1.ClientRectangle);
            // 32转1bit
            Bitmap newbitmap = bitmap.Clone(new Rectangle(0, 0, bitmap.Width, bitmap.Height), PixelFormat.Format1bppIndexed);

            //bitmap.Save("output\\scale_ruler_32bit.bmp", ImageFormat.Bmp);
            newbitmap.Save($"output\\KJ4B_RC1200_2H\\taose_{width}X{height}_{str_CMYK}_1bit.bmp", ImageFormat.Bmp);

            bitmap.Dispose();
            newbitmap.Dispose();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    startPosY = 0;
                    str_CMYK = "C";
                    break;
                case 1:
                    startPosY = height * 1;
                    str_CMYK = "M";

                    break;
                case 2:
                    startPosY = height * 2;
                    str_CMYK = "Y";

                    break;
                case 3:
                    startPosY = height * 3;
                    str_CMYK = "K";

                    break;
                default:
                    startPosY = 0;
                    str_CMYK = "C";

                    break;
            }
            pictureBox1.Invalidate();
        }
    }
}
