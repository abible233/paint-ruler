﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintRuler
{
    public partial class Rectanglejuxing : Form
    {
        const int nWidth= 400;
        const int nHeight = 400;

        public Rectanglejuxing()
        {
            InitializeComponent();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Size picture_size = new Size(nWidth, nHeight);
            pictureBox1.Size = picture_size;
            Font font = new Font("Arial", 90, FontStyle.Bold);

            Graphics g = e.Graphics;
            DrawFunc(g);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(nWidth, nHeight);
            using(Graphics g = Graphics.FromImage(bitmap))
            {
                DrawFunc(g);
            }
            pictureBox1.DrawToBitmap(bitmap, pictureBox1.ClientRectangle);
            // 32转1bit
            Bitmap newbitmap = bitmap.Clone(new Rectangle(0, 0, bitmap.Width, bitmap.Height), PixelFormat.Format1bppIndexed);
            //bitmap.Dispose();
            newbitmap.Save($"output\\Rectangle_{nWidth}x{nHeight}.bmp", ImageFormat.Bmp);

            newbitmap.Dispose();
        }
        private void DrawFunc(Graphics g)
        {
            g.Clear(Color.White);
            Pen pen = new Pen(Color.Black);
            //g.FillRectangle(Brushes.Black, 0, 0, nLength, nLength);
            //for (int i = 0; i < 20; i++)
            //{
            //    g.FillRectangle(Brushes.Black, i, i * 500, 800, 500);
            //    g.DrawString($"{i + 1}", font, Brushes.Black, 980, i * 500 + 250);
            //}
            //g.DrawLine(pen, 0, 0, nLength - 1, 0);
            //g.DrawLine(pen, 0, 0, 0, nHeight - 1);
            //g.DrawLine(pen, 0, nHeight - 1, nLength - 1, nHeight - 1);
            //g.DrawLine(pen, nLength - 1, 0, nLength - 1, nHeight - 1);


            // 渐变
            // 181 181 182
            g.FillRectangle(new LinearGradientBrush(new Point(nWidth, nHeight), new Point(nWidth, -1), Color.FromArgb(0, 0, 0), Color.FromArgb(255, 255, 255)), pictureBox1.ClientRectangle);

        }
    }
}
