﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace PaintRuler
{
    public partial class T3200Status : Form
    {
        private const int width = 3200;
        private const int height = 6000;

        private int StartPosX = 0;
        private int StartPosY = 0;

        private const int ColorNumber = 2;// 两色喷头
        private const int SingleColorWidth = width / ColorNumber;// 单个颜色的宽度


        private int AccumulatedHeight = 0;// 每个作用块的高度累加值

        private const int deviation_angle_area_height = 1000;// 角度区域总高度
        private const int angle_area_interval = 46;// 角度两块之间的间隔 1mm
        private const int angle_base_length = 151;// 角度基准线
        private const int angle_check_length = 250;// 角度校准线

        private const int interval = 64;// 一组小线段所占的高度，用于和nCount相乘算Y位置
        private const int short_line_length = 80;// 长短线看夹的情况，短线长度
        private const int long_line_length = 150;// 长短线看夹的情况，长线长度
        private const int tixing_space = 8;// 梯形线的间隔
        private const int tixing_length=120;// 梯形线的长度

        public T3200Status()
        {
            InitializeComponent();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Size picture_size = new Size(width, height);
            pictureBox1.Size = picture_size;

            Graphics g = e.Graphics;
            g.Clear(Color.White);
            Pen pen = new Pen(Color.Black);
            Font font = new Font("Arial", 112, FontStyle.Bold);
            Pen pen_dot = new Pen(Color.Black);
            pen_dot.DashStyle = System.Drawing.Drawing2D.DashStyle.Dot;

            int n = 0;
            while (n < ColorNumber)
            {
                StartPosY = height / ColorNumber * n;
                StartPosX = width / ColorNumber * n;
                AccumulatedHeight = 0;

                // 起始空白
                AccumulatedHeight += 30;

                // 深色块
                int height_unit = 120;// 单个功能块的高度
                for(int x = StartPosX; x < StartPosX + SingleColorWidth; x++)
                {
                    g.DrawLine(pen, x, StartPosY + AccumulatedHeight, x, StartPosY + AccumulatedHeight + height_unit);
                }
                // 深色块文字
                g.DrawString("1234567890", font, Brushes.White, StartPosX + 340, StartPosY + AccumulatedHeight - 22);
                AccumulatedHeight += height_unit;

                // 中深色块
                height_unit = 120;
                for (int x = StartPosX; x < StartPosX + SingleColorWidth; x+=2)
                {
                    g.DrawLine(pen, x, StartPosY + AccumulatedHeight, x, StartPosY + AccumulatedHeight + height_unit);
                }
                AccumulatedHeight += height_unit;

                // 浅深色块
                height_unit = 120;
                for (int x = StartPosX; x < StartPosX + SingleColorWidth; x += 2)
                {
                    g.DrawLine(pen_dot, x, StartPosY + AccumulatedHeight, x, StartPosY + AccumulatedHeight + height_unit);
                }
                AccumulatedHeight += height_unit;

                // 空白
                AccumulatedHeight += 150;

                // 粗横线
                g.DrawLine(pen, StartPosX, StartPosY + AccumulatedHeight, StartPosX + SingleColorWidth - 1, StartPosY + AccumulatedHeight);
                g.DrawLine(pen, StartPosX, StartPosY + AccumulatedHeight + 1, StartPosX + SingleColorWidth - 1, StartPosY + AccumulatedHeight + 1);
                g.DrawLine(pen, StartPosX, StartPosY + AccumulatedHeight + 2, StartPosX + SingleColorWidth - 1, StartPosY + AccumulatedHeight + 2);
                g.DrawLine(pen, StartPosX, StartPosY + AccumulatedHeight + 3, StartPosX + SingleColorWidth - 1, StartPosY + AccumulatedHeight + 3);
                AccumulatedHeight += 3;
                // 空白
                AccumulatedHeight += 27;
                // 横线
                g.DrawLine(pen, StartPosX, StartPosY + AccumulatedHeight, StartPosX + SingleColorWidth - 1, StartPosY + AccumulatedHeight);
                g.DrawLine(pen, StartPosX, StartPosY + AccumulatedHeight + 1, StartPosX + SingleColorWidth - 1, StartPosY + AccumulatedHeight + 1);
                AccumulatedHeight += 2;
                // 空白
                AccumulatedHeight += 28;
                // 横线
                g.DrawLine(pen, StartPosX, StartPosY + AccumulatedHeight, StartPosX + SingleColorWidth - 1, StartPosY + AccumulatedHeight);
                AccumulatedHeight += 1;
                // 空白
                AccumulatedHeight += 29;
                // 横虚线
                g.DrawLine(pen_dot, StartPosX, StartPosY + AccumulatedHeight, StartPosX + SingleColorWidth - 1, StartPosY + AccumulatedHeight);
                g.DrawLine(pen_dot, StartPosX, StartPosY + AccumulatedHeight + 1, StartPosX + SingleColorWidth - 1, StartPosY + AccumulatedHeight + 1);
                AccumulatedHeight += 2;
                // 空白
                AccumulatedHeight += 28;
                // 横虚线
                g.DrawLine(pen_dot, StartPosX, StartPosY + AccumulatedHeight, StartPosX + SingleColorWidth - 1, StartPosY + AccumulatedHeight);
                AccumulatedHeight += 1;
                // 空白
                AccumulatedHeight += 29;
                AccumulatedHeight += 100;
                // 阶梯线
                for (int i = StartPosX; i < StartPosX + SingleColorWidth; i += tixing_space)
                {
                    for (int j = 0; j < tixing_space; j++)
                    {
                        g.DrawLine(pen, i + j, StartPosY + AccumulatedHeight + tixing_length * j, i + j, StartPosY + AccumulatedHeight + tixing_length * (j + 1));
                    }
                }
                AccumulatedHeight += tixing_space * tixing_length;

                // 空白
                AccumulatedHeight += 170;

                // 单色角度线，短线夹长线
                // 短线夹长线
                for (int i = StartPosX; i < StartPosX + SingleColorWidth; i += 64)
                {
                    g.DrawLine(pen, i, StartPosY + AccumulatedHeight, i, StartPosY + AccumulatedHeight + short_line_length);
                    g.DrawLine(pen, i + 16, StartPosY + AccumulatedHeight, i + 16, StartPosY + AccumulatedHeight + long_line_length);
                    g.DrawLine(pen, i + 32, StartPosY + AccumulatedHeight, i + 32, StartPosY + AccumulatedHeight + short_line_length);
                }
                AccumulatedHeight += long_line_length;
                // 空白
                AccumulatedHeight += 50;
                for (int i = StartPosX; i < StartPosX + SingleColorWidth; i += 36)
                {
                    g.DrawLine(pen, i, StartPosY + AccumulatedHeight, i, StartPosY + AccumulatedHeight + short_line_length);
                    g.DrawLine(pen, i + 9, StartPosY + AccumulatedHeight, i + 9, StartPosY + AccumulatedHeight + long_line_length);
                    g.DrawLine(pen, i + 18, StartPosY + AccumulatedHeight, i + 18, StartPosY + AccumulatedHeight + short_line_length);
                }
                AccumulatedHeight += long_line_length;
                // 空白
                AccumulatedHeight += 50;

                for (int i = StartPosX; i < StartPosX + SingleColorWidth; i += 24)
                {
                    g.DrawLine(pen, i, StartPosY + AccumulatedHeight, i, StartPosY + AccumulatedHeight + short_line_length);
                    g.DrawLine(pen, i + 6, StartPosY + AccumulatedHeight, i + 6, StartPosY + AccumulatedHeight + long_line_length);
                    g.DrawLine(pen, i + 12, StartPosY + AccumulatedHeight, i + 12, StartPosY + AccumulatedHeight + short_line_length);
                }
                AccumulatedHeight += long_line_length;
                // 空白
                AccumulatedHeight += 170;


                int Offset = 150;// 第二段短线的起始偏移
                // 角度校准线
                if (n == 0)
                {
                    g.DrawLine(pen, StartPosX, StartPosY + AccumulatedHeight + height / ColorNumber,
                        StartPosX + SingleColorWidth - 1, StartPosY + AccumulatedHeight + height / ColorNumber);
                    for(int i=StartPosX;i< StartPosX + SingleColorWidth; i += 50)
                    {
                        g.DrawLine(pen, i, StartPosY + AccumulatedHeight + height / ColorNumber, i, StartPosY + AccumulatedHeight + height / ColorNumber + short_line_length);
                    }
                    for (int i = StartPosX; i < StartPosX + SingleColorWidth; i += 50)
                    {
                        g.DrawLine(pen, i, StartPosY + AccumulatedHeight + height / ColorNumber+ Offset, i, StartPosY + AccumulatedHeight + height / ColorNumber + short_line_length+ Offset);
                    }
                    g.DrawLine(pen, StartPosX, StartPosY + AccumulatedHeight + height / ColorNumber + short_line_length + Offset,
                         StartPosX + SingleColorWidth - 1, StartPosY + AccumulatedHeight + height / ColorNumber + short_line_length + Offset);
                }
                else if (n == 1)
                {
                    g.DrawLine(pen, StartPosX, StartPosY + AccumulatedHeight,
                        StartPosX + SingleColorWidth - 1, StartPosY + AccumulatedHeight);
                    for (int i = StartPosX; i < StartPosX + SingleColorWidth; i += 50)
                    {
                        g.DrawLine(pen, i, StartPosY + AccumulatedHeight+(long_line_length-(Offset-short_line_length))/2, i, StartPosY + AccumulatedHeight + (long_line_length - (Offset - short_line_length)) / 2+long_line_length);
                    }
                    g.DrawLine(pen, StartPosX, StartPosY + AccumulatedHeight + short_line_length + Offset,
                         StartPosX + SingleColorWidth - 1, StartPosY + AccumulatedHeight + short_line_length + Offset);
                }



                n++;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            pictureBox1.DrawToBitmap(bitmap, pictureBox1.ClientRectangle);
            // 32转1bit
            Bitmap newbitmap = bitmap.Clone(new Rectangle(0, 0, bitmap.Width, bitmap.Height), PixelFormat.Format1bppIndexed);
            bitmap.Dispose();
            newbitmap.Save($"output\\T3200Status.bmp", ImageFormat.Bmp);

            newbitmap.Dispose();
        }
    }
}
