﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PaintRuler
{
    public partial class chongdiezui : Form
    {
        private const int width = 640;
        private const int height = 640;

        public chongdiezui()
        {
            InitializeComponent();
        }

        private void chongdiezui_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            PictureBox picbox = (PictureBox)sender;
            Size size = new Size(width, height);
            picbox.Size = size;

            Graphics g = e.Graphics;
            g.Clear(Color.White);
            Pen pen = new Pen(Color.Black);
            SolidBrush brush = new SolidBrush(Color.Black);

            Random random = new Random();
            int randomNumber = 0;

            // 画随机长度的横线
            //for (int i = 0; i < height; i++)
            //{
            //    randomNumber = random.Next(width);// 生成0到宽之间的随机数
            //    g.DrawLine(pen, 0, i, randomNumber, i);
            //}

            // 画色块
            for (int i = 0; i < height; i++)
            {
                if (i == 0)
                {
                    for (int j = 0; j < width; j++)
                    {
                        if (random.Next(100) < 15)
                        {
                            g.FillRectangle(brush, j, i, 1, 1);
                        }
                    }
                }
                //else
                //{
                //    g.DrawLine(pen, 0, i, width - 1, i);
                //}
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bitmap bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            pictureBox1.DrawToBitmap(bitmap, pictureBox1.ClientRectangle);
            // 32转1bit
            Bitmap newbitmap = bitmap.Clone(new Rectangle(0, 0, bitmap.Width, bitmap.Height), PixelFormat.Format1bppIndexed);

            newbitmap.Save($"output\\重叠嘴模板\\chongdiezui_moban_{width}X{height}_1bit.bmp", ImageFormat.Bmp);

            bitmap.Dispose();
            newbitmap.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            pictureBox1.Invalidate();
        }
    }
}
